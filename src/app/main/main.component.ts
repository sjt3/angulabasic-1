import { Component } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {
  title = 'Nombre medicamento';
 // nombres = ["Paracetamol", "Solucion fisiologica", "Antibiotico"];

  public nombres = [
    {nom:'Paracetamol',cant:10},
    {nom: 'Antibiotico',cant:20},
    {nom: 'Solucion Fisiologica',cant:30}
    ];
   
}
